import sys
import argparse

import cx_Oracle


class DatabaseWriter:
    """
    Writes all strings from selected text file to the Oracle Database.
    Text file and db_writer.py must be in the same directory. 
    Use: python db_writer.py -d db_login/db_password@hostname -f filename -t table_name
    -s separator(default=',') -r range(default=10000)
    Help: python de_writer.py -h
    """
    def __init__(self, connect_db, filename, string_range, separator, table_name):
        self.connect_db = connect_db
        self.filename = filename
        self.string_range = int(string_range)
        self.separator = separator
        self.table_name = table_name

    def generate_query(self, line):
        num_of_columns = []
        for i in range((line.count(self.separator) + 1)):
            num_of_columns.append(':' + str(i+1))
        result = ', '.join(num_of_columns)
        query = "INSERT INTO {} VALUES ({})".format(self.table_name, result)
        return query, len(num_of_columns)

    def write_to_db(self):
        try:
            con = cx_Oracle.connect(self.connect_db)
        except cx_Oracle.DatabaseError as e:
            print('Connection error: ', e)
            return
        cur = con.cursor()
        lines_to_insert = []
        with open(self.filename, 'r') as f:
            line = f.readline()
            query, num_of_columns = self.generate_query(line)
            while line:
                for i in range(self.string_range):
                    if line:
                        if num_of_columns == line.count(self.separator) + 1:
                            lines_to_insert.append(line.strip().split(self.separator))
                        else:
                            print('String "{}" contains incorrect number of columns'.format(line.strip()))
                            line = (f.readline())
                            continue
                    else:
                        break
                    line = (f.readline())
                try:
                    cur.executemany(query, lines_to_insert)
                except cx_Oracle.DatabaseError as e:
                    print('Incorrect query: ', e)
                    return
                lines_to_insert = []
                con.commit()
        cur.close()
        con.close()
        print('Done')


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser._action_groups.pop()
    required = parser.add_argument_group('required arguments')
    optional = parser.add_argument_group('optional arguments')
    required.add_argument('-d', '--database_url', help='login/pass@hostname')
    required.add_argument('-f',  '--filename', help='Name of file with data')
    required.add_argument('-t', '--tablename', help='Name of table to INSERT')
    optional.add_argument('-r', '--range', default=10000, help="Number of strings per each commit. (Default: 10000)")
    optional.add_argument('-s', '--separator', default=',', help='Column separator. (Default: ",")')
    namespace = parser.parse_args(sys.argv[1:])

    connect_db = namespace.database_url
    filename = namespace.filename
    string_range = (int(namespace.range) if int(namespace.range) > 0 else 10000)
    separator = namespace.separator
    table_name = namespace.tablename

    writer = DatabaseWriter(connect_db, filename, string_range, separator, table_name)
    writer.write_to_db()
